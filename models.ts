export class Header {
  id: string;
  name: string;
  include: boolean;

  titleManager: TranslatableTextManager;

  constructor(data: HeaderData) {
    this.id = data.name;
    this.name = data.name;

    // @assumption - Only importing data defined as paper ballot
    const purpose = data.purpose.toLowerCase();
    this.include = (purpose === 'paper ballot' || purpose === 'both');
    this.titleManager = new TranslatableTextManager();
  }

  translate(data: LanguageData[]) {
    data.filter(item => {
    }).forEach(item => {
    })
  }
}

export class Choice{
  // static sequence = 10;
  id: string;
  name: string;
  sequence: number;
  type: string;
  profession: string;
  incumbent:boolean;
  partyName: string;
  contestId: string;
  order:string
  titleManager: TranslatableTextManager;
  titleManager2: TranslatableTextManager;
  constructor(data:ChoiceData){
    this.contestId=data.cstid;
    this.titleManager = new TranslatableTextManager();

    if(data.cndnme!==undefined){
      this.id= data.cndid;
      this.name= data.cndnme;
      this.profession= data.job;
      this.order= data.cndord;//base order for candidate for rotation type 1.
      this.partyName= data.baltag;
      this.titleManager.add(0, this.name, 'English', 'default');
      this.titleManager.add(1,this.partyName,'English','subtitle');
      if(this.profession!==undefined){
        this.titleManager.add(2, this.profession, 'English', 'subtitle');
      }
    }else{
      if(data.optvrb1!==undefined){
        this.id= data.qstoptid;
        this.name= data.optvrb1;
        this.titleManager.add(0, this.name, 'English', 'default');
      }
    }
    this.type='default';
    this.sequence= Number(data.cndord);
    // this.sequence= Choice.sequence;
    this.incumbent=false;
    if(this.profession==='Incumbent'){
      this.incumbent=true;
    }
    // Choice.sequence += 10;
  }
}

export class Contest{
  static sequence = 10;
  type: 'contest'|'measure'|'text';
  selections: number;
  writeins: number;
  sequence: number;
  termlength: number;
  text: TranslatableText[];
  titleManager: TranslatableTextManager;
  textManager: TranslatableTextManager;
  id: string;
  name: string;
  title: string;
  header: string;
  msg: string;
  question: string;
  questionId: string;
  rotoff: string;
  constructor(data:ContestData){
    this.id = data.cstid;
    this.name = (data.cstnme);
    while((this.name).match(/^\d/)){
      this.name = this.name.substr(1);
    }
    this.writeins = Number(data.numtoele);
    this.rotoff= data.rotoff;
    this.type= 'contest';
    this.titleManager = new TranslatableTextManager();
    this.textManager = new TranslatableTextManager();
    if(data.qstid!=='0'){
      this.questionId = data.qstid;
      this.type = 'measure';
    }
    this.titleManager.add(0, this.name, 'English', 'default');
    this.sequence = Contest.sequence;
    Contest.sequence += 10;
  }
  translate(data: LanguageData[]){
    data.filter(item=>{
      return item.cstid === this.id;
    }).forEach(item=>{
      const lg = item.language;
      const title = item.ballotcontestname;
      const subtitle = item.votefortext;

      if(lg.toLowerCase().substr(0,4)!=='engl'){
        if(title){
          this.titleManager.add(0, title, lg, 'default');
        }
      }
      if(subtitle){
        this.titleManager.add(1, subtitle, lg, 'subtitle')
      }
    })
  }
}

export class BallotStyle{
  // static ballotType = 1;
  ballotType: string;
  id: string;
  name: string;
  code: string;
  external_box_ids: string[] = [];
  external_precinct_ids: string[] = [];
  constructor(data: ballotStyleData){
    this.id = data.balstyid
    this.ballotType = data.balstynum;
    this.name = data.balstynme;
    this.code = data.balstynum;
    // this.ballotType = BallotStyle.ballotType;
    // BallotStyle.ballotType +=1;
  }
}

export class BallotContestMap {
  styles = new Map<string, Set<string>>();
  constructor(data: BallotContestData[]) {
    data.forEach(item => {
      let contests = this.styles.get(item.balstynme);
      if (!contests) {
        contests = new Set();
        this.styles.set(item.balstynme, contests);
      }
      contests.add(item.cstid);
    });
  }
  get(styleName:string){
    return this.styles.get(styleName);
  }
}

export class BallotPrecintMap{
  styles = new Map<string, Set<string>>();
  constructor(data: BallotPrecintData[]){
    //@assumption: pctid and pctsplid seem to be the same.
    data.forEach(item=>{
      let precincts = this.styles.get(item.balstyid);
      if(!precincts){
        precincts = new Set();
        this.styles.set(item.balstyid, precincts);
      }
      precincts.add(item.pctsplid);
    })
  }
  get(styleId:string){
    return this.styles.get(styleId);
  }
}

export class Precinct{
  pname: string;
  pid: string;
  psplitid: string;
  balstyext: string;
  constructor(data: precinctData){
    this.pname = data.pctnme;
    this.pid = data.pctid;
    this.psplitid = data.pctsplid;
    this.balstyext = data.balstyext;
  }
}

export class TranslatableTextManager {
  items: TranslatableText[] = [];

  add(i: number, text: string, lang: string, style: TranslatableStyle = 'default') {
    if (this.items[i] === undefined) {
      this.items[i] = new TranslatableText();
      this.items[i].style = style;
    }
    this.items[i].add(text, lang.toLowerCase());
  }

  getText(languages: Language[]): TranslatableText {
    return this.setLangCodes(this.items[0], languages);
  }

  getTextArray(languages: Language[]): TranslatableText[] {
    return this.items.filter(item => {
      return item.value != '' || Object.keys(item.translations).length > 0;
    }).map(item => {
      return this.setLangCodes(item, languages);
    });
  }

  // Convert language names to codes
  setLangCodes(text: TranslatableText, langs: Language[]): TranslatableText {
    langs.forEach(lang => {
      const partyName = lang.name.toLowerCase();
      if (text.translations[partyName]) {
        // @assumption - English is always defined like this
        if (partyName === 'english') {
          text.value = text.translations[partyName];
        } else {
          text.translations[lang.code] = text.translations[partyName];
        }
        delete text.translations[partyName];
      }
    });
    return text;
  }
}

export type TranslatableStyle = 'default'|'title'|'subtitle';
export type TranslatableFormat = 'default'|'text'|'style'|'html';

export class TranslatableText {
  value = '';
  format: TranslatableFormat = 'style';
  style: TranslatableStyle = 'default';
  translations: { [key: string]: string } = {};

  constructor(value: string = '') {
    this.value = value;
  }
  add(text: string, lang: string) {
    this.translations[lang] = text;
  }
}
export class Language{
  id: string;
  name: string;
  code: string;
  include: boolean;

  contestId: string;
  contestTitle: string;
  titleManager: TranslatableTextManager;

  constructor(data: LanguageData){
    this.id = data.languageid;
    this.name = data.language;
    switch (this.name.toLowerCase().substr(0,4)) {
      case 'engl':
        this.code = 'en';
        break;
      case 'span':
        this.code = 'es';
        break;
      case 'taga':
      case 'fili':
        this.code = 'tl';
        break;
      case 'chin':
      case 'mand':
      case 'cant':
        this.code = 'zh';
        break;
      default:
        this.code = this.name.toLowerCase();
        console.warn('Unknown language code for', this.name);
    }
    this.contestId = data.cstid
    if(data.ballotcontestname){
      this.contestTitle = data.ballotcontestname;
    }
    if(data.officemessage){
      this.contestTitle += '\n'+ data.officemessage;
    }
    this.titleManager = new TranslatableTextManager();
  }
  getHeaders(data: LanguageData[]){
    data.forEach(item=>{
      const lg = item.language;
      let header;
      if(item.banner){
        header = item.banner;
      }else if(item.header){
        header = item.header;
        if(item.subheader){
          header+= '. '+ item.subheader;
        }
      }
      if(item.header){
        header += '\n'+item.header;
        if(item.subheader){
          header += '. '+ item.subheader;
        }
      }
      if(header){
        this.titleManager.add(0, header, lg, 'default' );
      }
    })

  }
}

interface ElectionData {
  id: string;
  name: string;
  purpose: string;
}
interface LanguageData{
  cstid: string;
  languageid: string;
  reportingcontestname: string;
  language: string;
  banner: string;
  header: string;
  subheader: string;
  ballotcontestname: string;
  officemessage: string;
  votefortext: string;
}
interface BallotPrecintData{
  pctnum: string;
  pctnme: string;
  pctspl: string;
  pctsplid: string;
  balstynum: string;
  baltyp: string;
  balstyid: string;
}
interface BallotContestData{
  balstyid: string;
  baltyp: string;
  balstynme: string;
  cstid: string;
  cndrotnum: string; //rotation type 1(base order), 2(move one up) and 3
  cstsrtord: string;
  qstid: string;
  balstyrotnum: string;
}
interface ballotStyleData{
  balstynum: string;
  balstynme: string;
  balstyid: string;
}
interface precinctData{
  pctnum: string;
  pctide: string;
  pctnme: string;
  pctspl: string;
  brcpctnum: string;
  pctid: string;
  pctsplid: string;
  balstyext: string;
}
interface ContestData{
  rotoff: string;
  cstnme: string;
  cstid: string;
  numtoele: string;
  qstid: string;
  cstsrtord: string;
  prpemscstnum: string;
}
interface ChoiceData{
    cstid: string;
    cstnme: string;
    prpemscstnum: string;
    cndnme: string;
    optvrb1: string;
    job: string;
    baltag: string;
    vtgtgtid: string;
    tgtrelpos: string;
    cndord: string;
    cndid: string;
    qstoptid: string;
}
interface HeaderData {
  id: string;
  name: string;
  purpose: string;
  paperappearance: string;
  screenappearance: string;
}
