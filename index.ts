import {Sheet, WorkBook} from 'xlsx';
import {Choice, Contest, Header, Precinct, BallotStyle, BallotContestMap, BallotPrecintMap, Language} from './models';
import {CoreIO} from './coreio';
const fs = require('fs');
const path = require('path');
const xlsx = require('xlsx');
//official text/titles for ballots:
const directoryOfficial= './SF2018PrimaryDemocracyLiveExport/Audio\ Scripts/English/';
const fileOfficialCnd = 'qryRptCstTgtAudScrEN.XLS'//contest, candidate/option, party and profession.
const fileOfficialCst= 'qryRptCstAudScrEN.XLS'//contest, header, title, subtitle and text.
//support files for coreio:
const directory = './SF2018PrimaryDemocracyLiveExport/';
const filePrecincts= 'PctSpl.xlsx';
const fileContest = 'Cst.xlsx';
const fileballotStyle = 'BalSty.xlsx';//ballot styles
const fileballotContest ='BalTypCst.xlsx';
const fileballotPrecinct = 'BalStyPctSpl.xlsx'; //includes ballot type which is an original ballot style modified for rotation purpose
const fileChoices = 'VtgTgt.xlsx' //candidates and measure options
const fileMap = 'BalTypCst.xlsx'//maps contest to ballot style. Includes candidates' base rotation order for the contest.
//translation files:
const fileTranslations = 'SFC\ Contest\ Text\ and\ Translations.xls';

class Parser {

  workbooks: WorkBook[]=[];
  coreio: CoreIO;

  constructor(private dirOfficial: string, private dirSupport) {
    [this.dirOfficial, this.dirSupport].forEach(dir=>{
      fs.readdirSync(dir).forEach(file => {
        if(path.extname(file).toLowerCase() == '.xls' || path.extname(file).toLowerCase() == '.xlsx'){
          if(file[0] ==='.') return;
          const path= dir+file;
          this.workbooks[file] = xlsx.readFile(path);
        }
      });
    })
  }

  parse(accountId: string, electionId: string) {
    this.coreio = new CoreIO(accountId, electionId);

    const languages = this.getLanguages();
    languages.forEach(lg=>{
      try{
        this.coreio.addLanguage(lg.id,{
          name: lg.name,
          code: lg.code
        })
      }catch(error){}
    })

    const precincts = this.getPrecincts();
    precincts.forEach(precinct=>{
      try{
        this.coreio.addPrecinct(precinct.pid, precinct);
      }catch(error){}
    })

    const contests = this.getContests();
    contests.forEach(contest=>{
      if(contest.type === 'contest'){
        this.coreio.addOffice(contest.id,{
          titles: contest.titleManager.getTextArray(languages),
          text: contest.textManager.getTextArray(languages),
          num_selections: contest.selections,
          num_writeins: contest.writeins,
          sequence: contest.sequence,
            // @assumption - It's ok to default the year to this year.
          term_start: new Date().getFullYear(),
        })
      }else if(contest.type === 'measure'){
        this.coreio.addMeasure(contest.id, {
          titles: contest.titleManager.getTextArray(languages),
          text: contest.textManager.getTextArray(languages),
          num_selections: contest.selections,
          num_writeins: contest.writeins,
          sequence: contest.sequence
        });
      }
    });
    this.coreio.createBoxesFromOffices();
    this.coreio.createBoxesFromMeasures();

    const candidates = this.getChoices();
    candidates.forEach(choice=>{
      try{
        this.coreio.addCandidate(choice.id, {
          titles: choice.titleManager.getTextArray(languages),
          sequence: choice.sequence,
        });

      }catch(error){}
    })
    this.coreio.createOptionsFromCandidates();

    const rotationInfo = this.getSheetData(fileballotContest);
    rotationInfo.forEach(row=>{
      const ballotStyle = row.balstynum;
      const rotationSetNum = row.cndrotnum;
      const boxes = {}

      rotationInfo.forEach(r =>{
        if(r.cndrotnum==rotationSetNum){
          const rotation_contest_id = r.cstid;
          const options ={}
          candidates.forEach(candidate=>{
            if(candidate.contestId==rotation_contest_id){
              options[candidate.id]=candidate.sequence;
            }
          })
          boxes[rotation_contest_id] = options;
        }
      })
      console.log(boxes);
      this.coreio.addRotationSet(rotationSetNum, {
        external_boxes: boxes,
      })
    })

    candidates.forEach(c=>{
      this.coreio.mapOptionToBox(c.id, c.contestId);
    })

    const ballotStyles = this.getBallotStyles();
    const ballotContestMap = this.getBallotContests();
    const ballotPrecintMap = this.getBallotPrecincts();
    ballotStyles.forEach(style=>{
      const contest_ids = ballotContestMap.get(style.name);//set of ids
      const precinct_ids = ballotPrecintMap.get(style.id);
      this.coreio.addStyle(style.id, {
        name: style.name,
        code: style.code,
        external_box_ids: Array.from(contest_ids),
        external_precinct_ids: Array.from(precinct_ids),
      });
    });
    //translation files has the information for headers
    const headers = this.getLanguages();
    headers.forEach(header=>{
      // @assumption - It's ok to place headers -1 from the first contest found with that header
      let sequence =0;
      const firstContest = contests.find(contest => header.contestId === contest.id);
      if (firstContest) {
        sequence = firstContest.sequence - 1;
      }
      try{
        this.coreio.addBox('contest-'+header.contestId,{
          type: 'header',
          titles: header.titleManager.getTextArray(languages),
          sequence,
        })
      }catch(error){}
    })
    console.log("Done Parsing");
  }//end of parse method.

  output(outputPath: string) {
    fs.writeFileSync(outputPath, JSON.stringify(this.coreio.getData(), null, '  '));
    console.log("Done Writing");
  }

  getLanguages(): Language[]{
    const dataForHeaders = this.getSheetData(fileTranslations);
    return this.getSheetData(fileTranslations).map((data,i)=>{
      const language = new Language(data);
      language.getHeaders(dataForHeaders);
      return language;
    })
  }
  getContests(): Contest[]{
    const translatedData= this.getSheetData(fileTranslations);
    return this.getSheetData(fileContest).map((data,i) => {
      const contest = new Contest(data);
      contest.translate(translatedData);
      return contest;
    });
  }
  getBallotPrecincts(): BallotPrecintMap{
    const data = this.getSheetData(fileballotPrecinct);
    return new BallotPrecintMap(data);
  }
  getBallotContests(): BallotContestMap{
    const data = this.getSheetData(fileballotContest);
    return new BallotContestMap(data);
  }
  getBallotStyles(): BallotStyle[]{
    return this.getSheetData(fileballotStyle).map((data,i)=>{
      return new BallotStyle(data);
    })
  }
  getPrecincts(): Precinct[]{
    return this.getSheetData(filePrecincts).map((data,i)=>{
      if(data.pctnme!==undefined){
        return new Precinct(data);
      }
    })
  }
  getChoices(): Choice[]{
    return this.getSheetData(fileChoices).map((data,i) => {
      return new Choice(data);
    });
  }

  getSheetData(fileName:string): any {
    const sheet = this.getSheet(fileName);
    return xlsx.utils.sheet_to_json(sheet).map(data => {
      // Convert keys to normalized values
      const ret = {};
      for (let key in data) {
        const normalized = key.toLowerCase().replace(/[^a-z0-9]/g, '');
        ret[normalized] = data[key]
      }
      return ret;
    });

  }
  getSheet(fileName: string): Sheet {
    const workbook = this.workbooks[fileName];
    const sheetName = workbook.SheetNames[0];//@assumption:each file only contains one sheet.
    return workbook.Sheets[sheetName];
  }

}//end of class Parser.

let p = new Parser(directoryOfficial, directory);
p.parse('dev', '1');
p.output('./output/output.json');
